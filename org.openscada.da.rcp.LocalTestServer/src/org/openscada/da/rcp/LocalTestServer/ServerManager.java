/*
 * This file is part of the OpenSCADA project
 * 
 * Copyright (C) 2012 TH4 SYSTEMS GmbH (http://th4-systems.com)
 * Copyright (C) 2013 Jens Reimann (ctron@dentrassi.de)
 *
 * OpenSCADA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenSCADA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenSCADA. If not, see
 * <http://opensource.org/licenses/lgpl-3.0.html> for a copy of the LGPLv3 License.
 */

package org.openscada.da.rcp.LocalTestServer;

import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.openscada.core.ConnectionInformation;
import org.openscada.da.core.server.Hive;
import org.openscada.da.server.net.Exporter;
import org.openscada.utils.lifecycle.LifecycleAware;

public class ServerManager
{
    public class Entry
    {
        private final List<ConnectionInformation> endpoints;

        private final String description;

        private final Hive hive;

        private final List<LifecycleAware> exporters = new LinkedList<LifecycleAware> ();

        public Entry ( final String description, final Hive hive, final List<ConnectionInformation> endpoints ) throws Exception
        {
            this.description = description;
            this.hive = hive;
            this.endpoints = new LinkedList<ConnectionInformation> ();

            hive.start ();

            for ( final ConnectionInformation endpoint : endpoints )
            {
                if ( endpoint.getDriver ().equalsIgnoreCase ( "net" ) )
                {
                    final Exporter exporter = new Exporter ( hive, endpoint );
                    exporter.start ();
                    this.endpoints.addAll ( exporter.getStartedConnections () );
                    this.exporters.add ( exporter );
                }
                else if ( endpoint.getDriver ().equalsIgnoreCase ( "ngp" ) )
                {
                    final org.openscada.da.server.ngp.Exporter exporter = new org.openscada.da.server.ngp.Exporter ( hive, endpoint );
                    exporter.start ();
                    this.endpoints.addAll ( convert ( endpoint.getInterface (), exporter.getStartedAddresses () ) );
                    this.exporters.add ( exporter );
                }
            }
        }

        private Collection<ConnectionInformation> convert ( final String interfaceName, final Set<InetSocketAddress> startedAddresses )
        {
            final Collection<ConnectionInformation> result = new LinkedList<ConnectionInformation> ();

            for ( final InetSocketAddress address : startedAddresses )
            {
                final String target = address.getAddress ().getHostAddress ().replace ( "%", "%25" ); // for IPv6

                final ConnectionInformation ci;
                if ( target.indexOf ( ':' ) >= 0 )
                {
                    ci = ConnectionInformation.fromURI ( String.format ( "%s:ngp://[%s]:%s", interfaceName, target, address.getPort () ) );
                }
                else
                {
                    ci = ConnectionInformation.fromURI ( String.format ( "%s:ngp://%s:%s", interfaceName, target, address.getPort () ) );
                }
                if ( ci != null )
                {
                    result.add ( ci );
                }
            }

            return result;
        }

        public List<ConnectionInformation> getEndpoints ()
        {
            return this.endpoints;
        }

        public String getDescription ()
        {
            return this.description;
        }

        public void dispose ()
        {
            for ( final LifecycleAware exporter : this.exporters )
            {
                try
                {
                    exporter.stop ();
                }
                catch ( final Exception e )
                {
                    // FIXME: log
                }
            }

            try
            {
                this.hive.stop ();
            }
            catch ( final Exception e )
            {
                // FIXME: log exception
            }

            this.exporters.clear ();
        }
    }

    public interface ServerListener
    {
        public void serverAdded ( Entry entry );

        public void serverRemoved ( Entry entry );
    }

    private final Set<ServerListener> listeners = new LinkedHashSet<ServerManager.ServerListener> ();

    private final Map<Hive, Entry> entries = new HashMap<Hive, Entry> ();

    public ServerManager ()
    {
    }

    public void startServer ( final Hive hive, final List<ConnectionInformation> connectionInformation, final String description ) throws Exception
    {
        if ( this.entries.containsKey ( hive ) )
        {
            throw new AlreadyStartedException ();
        }

        final Entry entry = new Entry ( description, hive, connectionInformation );
        this.entries.put ( hive, entry );
        fireAdd ( entry );
    }

    public void stopServer ( final Hive hive )
    {
        final Entry entry = this.entries.remove ( hive );
        if ( entry != null )
        {
            entry.dispose ();
            fireRemove ( entry );
        }
    }

    protected void fireAdd ( final Entry entry )
    {
        for ( final ServerListener listener : this.listeners )
        {
            listener.serverAdded ( entry );
        }
    }

    protected void fireRemove ( final Entry entry )
    {
        for ( final ServerListener listener : this.listeners )
        {
            listener.serverRemoved ( entry );
        }
    }

    public void addListener ( final ServerListener listener )
    {
        if ( this.listeners.add ( listener ) )
        {
            // transmit cache
            for ( final Entry entry : this.entries.values () )
            {
                listener.serverAdded ( entry );
            }
        }
    }

    public void removeListener ( final ServerListener listener )
    {
        this.listeners.remove ( listener );
    }

    public void dispose ()
    {
        final Collection<Entry> entries = new ArrayList<ServerManager.Entry> ( this.entries.values () );
        for ( final Entry entry : entries )
        {
            entry.dispose ();
        }

        entries.clear ();
        this.listeners.clear ();
    }
}
